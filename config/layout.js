// Default Helmet props
export default Object.freeze({
  htmlAttributes: {lang: 'en'},
  title: 'Test Table',
  defaultTitle: 'Test Table',
  titleTemplate: '%s - Test Table',
  meta: [
    {charset: 'utf-8'},
    {name: 'viewport', content: 'width=device-width, initial-scale=1'}
  ],
  link: [
    {rel: 'shortcut icon', href: '/favicon.ico'},
  ],
  script: [],
  style: []
})
