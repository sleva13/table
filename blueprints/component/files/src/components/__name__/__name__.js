import React, {PropTypes} from 'react'
import {pure} from 'recompose'
import s from './<%= pascalEntityName %>.scss'

export const <%= pascalEntityName %> = pure(() =>
  <div></div>
)

<%= pascalEntityName %>.propTypes = {
}

export default <%= pascalEntityName %>
