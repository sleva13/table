import {injectReducer} from 'store/reducers'
// import {injectSagas} from 'store/sagas'

export default (store) => ({
  path: '<%= dashesEntityName %>',
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const <%= pascalEntityName %> = require('./containers/<%= pascalEntityName %>Container').default

      const reducer = require('./modules/<%= pascalEntityName %>').default
      injectReducer(store, {key: '<%= pascalEntityName %>', reducer})

      // const <%= camelEntityName %> = require('./modules/<%= pascalEntityName %>').default
      // injectSagas(store, {key: '<%= pascalEntityName %>', sagas: [<%= camelEntityName %>]})

      cb(null, <%= pascalEntityName %>)
    }, '<%= pascalEntityName %>')
  }
})
