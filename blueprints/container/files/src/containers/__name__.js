import {connect} from 'react-redux'
import <%= pascalEntityName %> from 'components/<%= pascalEntityName %>/<%= pascalEntityName %>'

const mapDispatchToProps = {}

const mapStateToProps = (state) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(<%= pascalEntityName %>)
