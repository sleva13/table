import createReducer from 'utils/createReducer'
// ------------------------------------
// Constants
// ------------------------------------
export const CONSTANT = '<%= pascalEntityName %>.CONSTANT'

// ------------------------------------
// Actions
// ------------------------------------

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {}

export default createReducer(initialState, {
})
