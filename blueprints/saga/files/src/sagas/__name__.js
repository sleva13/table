import {take, call, put} from 'redux-saga/effects'

export default () => {
  function * worker(data) {

  }

  function * watcher() {
    while (true) {
      const data = yield take() // ACTION name
      yield call(worker, data)
    }
  }

  return {
    watcher,
    worker
  }
}
