import React, { PropTypes } from 'react'
import { pure } from 'recompose'
import s from './Header.scss'

const Header = pure(({ username, changeUsername }) =>
  <div className={s.header}>
    <div className={s.usernameWrapper}>
      <span>Username:</span>
      <input
        className={s.username}
        type='text'
        value={username}
        onChange={(e) => changeUsername(e.target.value)}
      />
    </div>
  </div>
)

Header.propTypes = {
  username: PropTypes.string,
  changeUsername: PropTypes.func,
}

export default Header
