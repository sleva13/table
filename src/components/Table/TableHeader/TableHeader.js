import React, { PropTypes } from 'react'
import { pure } from 'recompose'
import cn from 'classnames'
import s from './TableHeader.scss'

const TableHeader = pure(({ columns, sortBy, sortable }) =>
  <div className={s.header}>
    {columns.map((column, i) =>
      <a
        key={i}
        className={cn(sortable.column === column.key && (sortable.reverse ? s.desc : s.asc), s.headerCell)}
        onClick={() => sortBy(column.key)}
      >
        {column.title}
      </a>
    )}
  </div>
)

TableHeader.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  sortBy: PropTypes.func.isRequired,
  sortable: PropTypes.object.isRequired,
}

export default TableHeader
