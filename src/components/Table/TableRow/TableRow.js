import React, { PropTypes } from 'react'
import { pure } from 'recompose'
import cn from 'classnames'
import s from './TableRow.scss'

const TableRow = pure(({ className, columns, row, highlight }) =>
  <div className={cn(s.row, highlight && row[ highlight.column ] === highlight.text && s.highlight)}>
    {columns.map((column, i) =>
      <div key={i} className={s.cell}>
        {column.render ? column.render(row[ column.key ]) : row[ column.key ]}
      </div>
    )}
  </div>
)

TableRow.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  row: PropTypes.object.isRequired,
  highlight: PropTypes.object,
}

export default TableRow
