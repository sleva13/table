import React, { PropTypes } from 'react'
import { pure } from 'recompose'
import { TableHeader, TableRow } from './'
import s from './Table.scss'

const Table = pure(({ actions, data, columns, sortable, sortBy, highlight }) =>
  columns && data ? (
    <div>
      {actions}
      <TableHeader
        columns={columns}
        sortBy={sortBy}
        sortable={sortable}
      />
      <div className={s.body}>
        {data.map((row, i) =>
          <TableRow
            key={i}
            row={row}
            columns={columns}
            highlight={highlight}
          />
        )}
      </div>
    </div>
  ) : null
)

Table.propTypes = {
  actions: PropTypes.node,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  sortBy: PropTypes.func.isRequired,
  sortable: PropTypes.object.isRequired,
  highlight: PropTypes.object,
}

export default Table
