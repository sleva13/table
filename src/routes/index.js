import CoreLayout from '../layouts/CoreLayout/CoreLayout'
import Posts from './Posts'

/*  Note: Instead of using JSX, we recommend using react-router
 PlainRoute objects to build route definitions.   */
export const createRoutes = (store) => ({
  path: '/',
  component: CoreLayout,
  indexRoute: Posts(store),
  childRoutes: [
    {
      path: '*',
      onEnter: (state, replace) => {
        replace('/')
      }
    }
  ]
})

export default createRoutes
