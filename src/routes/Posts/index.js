import { injectReducer } from 'store/reducers'
import { injectSagas } from 'store/sagas'

export default (store) => ({
  getComponent (nextState, cb) {
    require.ensure([], (require) => {
      const Posts = require('./containers/PostsContainer').default
      const reducer = require('./modules/Posts').default
      injectReducer(store, { key: 'Posts', reducer })

      const getPosts = require('./modules/getPosts').default
      injectSagas(store, { key: 'Posts', sagas: [ getPosts ] })

      cb(null, Posts)
    }, 'Posts')
  }
})
