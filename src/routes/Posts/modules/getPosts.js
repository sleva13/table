import { take, call, put } from 'redux-saga/effects'
import { request } from 'sagas/api'
import { GET_POSTS_REQUEST } from './Posts'

export default () => {

  function * worker ({ query, responseSuccess, responseFailure }) {
    const body = {
      method: 'GET',
      query
    }

    const { res, err } = yield call(request, '/posts.json', body)

    if (res) {
      yield put(responseSuccess(res))
    } else {
      yield put(responseFailure(err))
    }
  }

  function * watcher () {
    while (true) {
      const form = yield take(GET_POSTS_REQUEST)
      yield call(worker, form)
    }
  }

  return {
    watcher,
    worker
  }
}
