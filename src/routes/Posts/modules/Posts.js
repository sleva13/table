import createReducer from 'utils/createReducer'
import compare from 'utils/compare'
import moment from 'moment'
import { reset } from 'redux-form'
import constants from 'config/constants.yml'

// ------------------------------------
// Constants
// ------------------------------------
export const GET_POSTS_REQUEST = 'Posts.GET_REQUEST'
export const GET_POSTS_SUCCESS = 'Posts.GET_SUCCESS'
export const GET_POSTS_FAILURE = 'Posts.GET_FAILURE'

export const CHANGE_PAGE = 'Posts.CHANGE_PAGE'
export const CHANGE_PAGE_SIZE = 'Posts.CHANGE_PAGE_SIZE'
export const SORT_BY = 'Posts.SORT_BY'
export const FILTER_BY = 'Posts.FILTER_BY'

export const ADD_POST = 'Posts.ADD'

// ------------------------------------
// Actions
// ------------------------------------
export const getPosts = () => (dispatch, getState) => {
  const { page, pageSize, sortable, filterable } = getState().Posts
  let query = {
    page,
    pageSize,
    sort_by: `${sortable.column},${sortable.reverse ? 'desc' : 'asc'}`
  }
  if (filterable.column && filterable.search) {
    query.filter_by = `${filterable.column},${filterable.search}`
  }
  dispatch({
    query,
    type: GET_POSTS_REQUEST,
    responseSuccess: getPostsSuccess,
    responseFailure: getPostsFailure
  })
}

export const getPostsSuccess = (posts) => (dispatch, getState) => {
  // Mocked API
  // pagination, filtering and sorting data on frontend side
  const { page, pageSize, sortable, filterable, addedPosts } = getState().Posts
  // workaround as we don't have backend to store posts
  posts.results = posts.results.concat(addedPosts)
  if (sortable) {
    posts.results = posts.results.sort((a, b) => compare(a, b, sortable.column))
    if (sortable.reverse) {
      posts.results = posts.results.reverse()
    }
  }
  if (filterable.column) {
    posts.results = posts.results.filter(item => {
      if (typeof item[ filterable.column ] === 'number') {
        if (filterable.search !== '') {
          return item[ filterable.column ] === +filterable.search
        }
        return true
      } else {
        return item[ filterable.column ].toLowerCase().includes(filterable.search.toLowerCase())
      }
    })
  }
  posts.count = posts.results.length
  const itemsCountPerPage = page * pageSize
  posts.results = posts.results.slice(itemsCountPerPage, itemsCountPerPage + pageSize)
  dispatch({ type: GET_POSTS_SUCCESS, posts })
}

export const getPostsFailure = (error) => ({ type: GET_POSTS_FAILURE })

export const changePage = ({ selected }) => dispatch => {
  dispatch({ type: CHANGE_PAGE, page: selected })
  dispatch(getPosts())
}

export const changePageSize = ({ value }) => dispatch => {
  dispatch({ type: CHANGE_PAGE_SIZE, pageSize: value })
  dispatch(getPosts())
}

export const sortBy = (column) => (dispatch, getState) => {
  const { sortable } = getState().Posts
  // set desc direction as default
  dispatch({ type: SORT_BY, sortable: { column, reverse: sortable.column === column ? !sortable.reverse : true } })
  dispatch(getPosts())
}

export const filterBy = (property) => (dispatch, getState) => {
  let filterable = { ...getState().Posts.filterable, ...property }
  dispatch({ type: FILTER_BY, filterable })
  if (filterable.column) {
    dispatch(getPosts())
  }
}

export const addPost = (post) => (dispatch, getState) => {
  const { postsCount } = getState().Posts
  const { username } = getState().User
  post = {
    id: postsCount + 1,
    likes: 0,
    views: 0,
    username,
    created_at: moment().format('YYYY-MM-DD'),
    ...post
  }
  dispatch({ type: ADD_POST, post })
  dispatch(reset('NewPost'))
  dispatch(getPosts())
}

// ------------------------------------
// Reducer
// ------------------------------------
export const initialState = {
  loading: false,
  posts: [],
  postsCount: 0,
  page: 0,
  pageSize: 5,
  sortable: constants.sortable,
  filterable: { search: '', column: null },
  // store added items in reducers because there is no api
  addedPosts: []
}

export default createReducer(initialState, {
  [GET_POSTS_REQUEST]: (state, action) => ({
    loading: true
  }),
  [GET_POSTS_SUCCESS]: (state, { posts }) => ({
    loading: false,
    posts: posts.results,
    postsCount: posts.count
  }),
  [CHANGE_PAGE]: (state, { page }) => ({
    page
  }),
  [CHANGE_PAGE_SIZE]: (state, { pageSize }) => ({
    // reset page when changing page size
    page: 0,
    pageSize
  }),
  [SORT_BY]: (state, { sortable }) => ({
    // reset page when sorting
    page: 0,
    sortable
  }),
  [FILTER_BY]: (state, { filterable }) => ({
    // reset page when filtering
    page: 0,
    filterable
  }),
  [ADD_POST]: (state, { post }) => ({
    addedPosts: [ ...state.addedPosts, post ]
  })
})
