import React, { Component, PropTypes } from 'react'
import Helmet from 'react-helmet'
import { Table, TableActions } from 'components'
import ReactPaginate from 'react-paginate'
import Select from 'react-select-me'
import Add from 'static/add.svg'
import NewPost from './NewPost'
import moment from 'moment'
import s from './Posts.scss'
import constants from 'config/constants.yml'

const POSTS_COLUMNS = [
  {
    key: 'id',
    title: 'ID'
  },
  {
    key: 'username',
    title: 'Username'
  },
  {
    key: 'post_title',
    title: 'Post title'
  },
  {
    key: 'views',
    title: 'Views'
  },
  {
    key: 'likes',
    title: 'Likes'
  },
  {
    key: 'created_at',
    title: 'Created at',
    render: (date) => moment(date, 'YYYY-MM-DD').format('YYYY-MM-DD')
  }
]

class Posts extends Component {
  static propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object),
    loading: PropTypes.bool,
    page: PropTypes.number,
    pageSize: PropTypes.number,
    postsCount: PropTypes.number,
    changePage: PropTypes.func,
    changePageSize: PropTypes.func,
    sortBy: PropTypes.func,
    sortable: PropTypes.object,
  }

  state = {
    addingPost: false
  }

  componentWillMount () {
    this.props.getPosts()
  }

  toggleAddingPost (addingPost) {
    this.setState({ addingPost })
  }

  render () {
    const {
      posts,
      page,
      pageSize,
      postsCount,
      changePage,
      changePageSize,
      sortBy,
      sortable,
      filterBy,
      filterable,
      username,
      addPost
    } = this.props
    const { addingPost } = this.state
    return (
      <div className={s.postsWrapper}>
        <Helmet title='Posts'/>
        <section className={s.posts}>
          <header className={s.header}>
            <h2>Posts</h2>
            <button className={s.addPost} onClick={() => this.toggleAddingPost(true)}>
              <Add/>
              <span>Add new post</span>
            </button>
          </header>
          <NewPost
            onSubmit={addPost}
            open={addingPost}
            onClose={() => this.toggleAddingPost(false)}
          />
          <Table
            data={posts}
            columns={POSTS_COLUMNS}
            sortable={sortable}
            sortBy={sortBy}
            highlight={{ column: 'username', text: username }}
            actions={
              <div className={s.actions}>
                <ReactPaginate
                  pageCount={postsCount / pageSize}
                  pageRangeDisplayed={1}
                  containerClassName={s.pagination}
                  pageClassName={s.page}
                  pageLinkClassName={s.pageLink}
                  activeClassName={s.active}
                  previousClassName={s.previous}
                  nextClassName={s.next}
                  previousLinkClassName={s.previousLink}
                  nextLinkClassName={s.nextLink}
                  disabledClassName={s.disabled}
                  breakClassName={s.break}
                  onPageChange={changePage}
                  forcePage={page}
                />
                <div className={s.filter}>
                  <span>Filter by:</span>
                  <Select
                    options={POSTS_COLUMNS}
                    value={filterable.column}
                    onChange={({ key }) => filterBy({ column: key })}
                    placeholder='Select column'
                    valueKey='key'
                    labelKey='title'
                    s={s}
                  />
                  <input
                    className={s.filterInput}
                    type='text'
                    value={filterable.search}
                    onChange={(e) => filterBy({ search: e.target.value })}
                  />
                </div>
                <div className={s.pageSize}>
                  <span>Items per page:</span>
                  <Select
                    options={constants.pageSizes}
                    value={pageSize}
                    onChange={changePageSize}
                    s={s}
                  />
                </div>
              </div>
            }
          />
        </section>
      </div>
    )
  }
}

export default Posts
