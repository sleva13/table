import React, { PropTypes } from 'react'
import { pure } from 'recompose'
import { Field, reduxForm } from 'redux-form'
import s from './NewPost.scss'

const validate = values => {
  const errors = {}
  if (!values.post_title) {
    errors.post_title = 'Required'
  }
  return errors
}

const NewPost = pure(({ handleSubmit, open, onClose }) =>
  open ? (
    <form onSubmit={handleSubmit} className={s.newPost}>
      <div className={s.postTitleWrapper}>
        <span>Post title:</span>
        <Field
          name='post_title'
          className={s.postTitle}
          component='input'
          type='text'
        />
      </div>
      <div className={s.buttons}>
        <button type='button' className={s.cancelBtn} onClick={onClose}>Cancel</button>
        <button type='submit' className={s.addBtn}>Add</button>
      </div>
    </form>
  ) : null
)

NewPost.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
}

export default reduxForm({
  form: 'NewPost',
  validate
})(NewPost)
