import { connect } from 'react-redux'
import { getPosts, changePage, changePageSize, sortBy, filterBy, addPost } from '../modules/Posts'
import Posts from '../components/Posts'

const mapActionCreators = {
  getPosts,
  changePage,
  changePageSize,
  sortBy,
  filterBy,
  addPost
}

const mapStateToProps = (state) => ({
  ...state.Posts,
  ...state.User
})

export default connect(mapStateToProps, mapActionCreators)(Posts)
