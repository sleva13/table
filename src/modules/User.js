import createReducer from 'utils/createReducer'
import { cookiesGet, cookiesSet } from 'redux-cookies'
import constants from 'config/constants.yml'

// ------------------------------------
// Constants
// ------------------------------------
export const LOGGED_IN = 'User.LOGGED_IN'
export const NOT_LOGGED_IN = 'User.NOT_LOGGED_IN'

export const CHANGE_USERNAME = 'User.CHANGE_USERNAME'

// ------------------------------------
// Actions
// ------------------------------------

export const initializeUser = () => dispatch => {
  const username = dispatch(cookiesGet('username'))
  if (username) {
    dispatch({ type: LOGGED_IN, username })
  } else {
    dispatch({ type: NOT_LOGGED_IN })
  }
}

export const changeUsername = (username) => dispatch => {
  dispatch(cookiesSet('username', username, { expires: constants.week }))
  dispatch({ type: CHANGE_USERNAME, username })
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  username: 'john'
}

export default createReducer(initialState, {
  [LOGGED_IN]: (state, { username }) => ({
    username
  }),
  [CHANGE_USERNAME]: (state, { username }) => ({
    username
  })
})

