import { connect } from 'react-redux'
import { changeUsername } from 'modules/User'
import Header from 'components/Header/Header'

const mapActionCreators = {
  changeUsername
}

const mapStateToProps = (state) => ({
  ...state.User
})

export default connect(mapStateToProps, mapActionCreators)(Header)
