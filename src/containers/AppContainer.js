import React, { Component, PropTypes } from 'react'
import { Router } from 'react-router'
import { connect, Provider } from 'react-redux'
import Helmet from 'react-helmet'
import defaultLayout from '../../config/layout'
import clone from 'clone'
import { initializeUser } from 'modules/User'

const mapActionCreators = {
  initializeUser
}

@connect(null, mapActionCreators)
class AppContainer extends Component {
  static propTypes = {
    layout: PropTypes.object,
    history: PropTypes.object.isRequired,
    routes: PropTypes.object.isRequired,
    routerKey: PropTypes.number,
    store: PropTypes.object.isRequired
  }

  componentWillMount () {
    this.props.initializeUser()
  }

  render () {
    const { layout, history, routes, routerKey, store } = this.props
    return (
      <Provider store={store}>
        <div>
          <Helmet {...Object.assign(clone(defaultLayout), layout)}/>
          <Router history={history} children={routes} key={routerKey}/>
        </div>
      </Provider>
    )
  }
}

export default AppContainer
