import { fork } from 'redux-saga/effects'

// inject your sync sagas here
const syncSagas = []

export const makeRootSaga = (sagas = syncSagas) => {
  return function *rootSaga () {
    yield sagas.map(saga => fork(saga().watcher))
  }
}

export const injectSagas = (store, { key, sagas }) => {
  if (store.asyncSagas[ key ]) {
    return
  }
  store.asyncSagas[ key ] = sagas
  store.runSaga(makeRootSaga(sagas))
}

export default makeRootSaga
