import React from 'react'
import { pure } from 'recompose'
import Header from 'containers/HeaderContainer'
import 'styles/core.scss'
import 'mock/posts.json'

export const CoreLayout = pure(({ children }) =>
  <div>
    <Header/>
    <div>{children}</div>
  </div>
)

CoreLayout.propTypes = {
  children: React.PropTypes.element.isRequired
}

export default CoreLayout
