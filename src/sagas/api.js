import { call } from 'redux-saga/effects'
import fetch from 'isomorphic-fetch'

const decodeUri = query => {
  const esc = encodeURIComponent
  return '?' + Object.keys(query).map(k => esc(k) + '=' + esc(query[ k ])).join('&')
}

const prepareRequestBody = (body, jsonRequest) => {
  if (jsonRequest) {
    return JSON.stringify(body)
  } else {
    let formDataBody = new FormData()
    Object.keys(body).forEach((key) => {
      formDataBody.append(key, body[ key ])
    })
    return formDataBody
  }
}

function prepareRequestHeaders (headers = {}, jsonRequest) {
  if (jsonRequest) {
    headers[ 'Content-Type' ] = 'application/json'
  }
  return headers
}

export function * request (url, data, jsonRequest = true) {
  let query = ''
  if (data.method === 'GET' && data.query) {
    query = decodeUri(data.query)
  }
  data.body = prepareRequestBody(data.body, jsonRequest)
  data.headers = prepareRequestHeaders(data.headers, jsonRequest)
  const resp = yield call(fetch, 'http://localhost:3000' + url + query, data)

  try {
    const responseBody = yield resp.json()
    return resp.ok ? { res: responseBody } : { err: responseBody }
  } catch (err) {
    return resp.ok ? { res: {} } : { err: {} }
  }
}

